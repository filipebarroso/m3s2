import 'package:app_francesinha/public/features/detail/presentation/detail_simple_page.dart';
import 'package:flutter/material.dart';

class RestaurantApp extends StatelessWidget {
  const RestaurantApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Restaurant App',
      theme: ThemeData(
        primarySwatch: Colors.red,
        textTheme: myTextTheme(),
        cardTheme: const CardTheme(
          color: Colors.red,
        ),
      ),
      // home: HomePage(
      //   homeRepository: HomeRepository(FoodService()),
      // ),
      home: DetailSimplePage(),
    );
  }

  TextTheme myTextTheme() {
    return const TextTheme(
      titleLarge: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
        color: Colors.black,
      ),
      titleMedium: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.normal,
        fontStyle: FontStyle.italic,
        color: Colors.grey,
      ),
      displayMedium: TextStyle(
        fontSize: 40,
        fontWeight: FontWeight.bold,
        color: Colors.black,
      ),
    );
  }
}
