import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';
import 'package:http/http.dart';

const host = 'francesinha-api-run-slnsbzbtxa-lm.a.run.app';

class ServiceManager {
  late final Client _client;

  ServiceManager() {
    _client = Client();
  }

  Future<FoodItemEntity> getFrancesinha(int id) async {
    final uri = Uri.https(host, 'francesinha/$id');
    final response = await _client.get(uri);
    return FoodItemEntity.fromRawJson(response.body);
  }

  Future<List<FoodItemEntity>> getFrancesinhas() async {
    final uri = Uri.https(host, 'francesinha');
    final response = await _client.get(uri);
    return francesinhasFromJson(response.body);
  }

  void close() {
    _client.close();
  }
}
