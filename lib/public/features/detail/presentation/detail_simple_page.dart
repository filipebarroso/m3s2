import 'package:app_francesinha/public/features/detail/application/service_manager.dart';
import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';
import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:app_francesinha/public/features/home/presentation/widgets/list_menu_widget.dart';
import 'package:flutter/material.dart';

class DetailSimplePage extends StatelessWidget {
  DetailSimplePage({super.key});

  final service = ServiceManager();

  // {"id":2,"name":"Francesinha","description":"Do veniam excepteur eu velit velit consequat consectetur id sunt voluptate occaecat aliqua laboris.","price":15}

  @override
  Widget build(BuildContext context) {
    final francesinha = service.getFrancesinhas();

    return Scaffold(
      body: FutureBuilder(
        future: francesinha,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(child: Text('Error'));
          } else if (snapshot.hasData) {
            final francesinhas = snapshot.data as List<FoodItemEntity>;

            return ListMenuWidget(
              items: francesinhas
                  .map(
                    (entity) => FoodItem.fromEntity(entity),
                  )
                  .toList(),
            );
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return const Center(
              child: Text('No data'),
            );
          }
        },
      ),
    );
  }
}



















// SizedBox.expand(
      //   child: SingleChildScrollView(
      //     child: Column(
      //       children: [
      //         const Text('Francesinhas'),
      //         FutureBuilder(
      //           future: francesinhas,
      //           builder: (context, snapshot) {
      //             if (snapshot.hasError) {
      //               return const Text('Error');
      //             } else {
      //               final francesinhas = snapshot.data as List<Francesinha>;

      //               return Column(
      //                 children: francesinhas.map(
      //                   (francesinha) {
      //                     return ListTile(
      //                       title: Text(francesinha.name),
      //                       subtitle: Text(francesinha.description),
      //                     );
      //                   },
      //                 ).toList(),
      //               );
      //             }
      //           },
      //         ),
      //       ],
      //     ),
      //   ),
      // )