import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';

class FoodItem {
  const FoodItem({
    required this.image,
    required this.title,
    required this.subtitle,
    required this.price,
  });

  final String image;
  final String title;
  final String subtitle;
  final String price;

  static FoodItem fromEntity(FoodItemEntity entity) {
    return FoodItem(
      image:
          'images/francesinha_${entity.id < 10 ? '0${entity.id}' : entity.id}.png',
      title: entity.name,
      subtitle: entity.description,
      price: '\$${entity.price.toString()}',
    );
  }
}
