import 'package:flutter/material.dart';

class ListMenuItemWidget extends StatelessWidget {
  const ListMenuItemWidget({
    super.key,
    this.imagePath,
    required this.title,
    required this.subtitle,
    required this.price,
  });

  final String? imagePath;
  final String title;
  final String subtitle;
  final String price;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Card(
        color: Colors.white,
        margin: const EdgeInsets.symmetric(horizontal: 16),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(21)),
        ),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: SizedBox(
                height: 120,
                width: 120,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(21)),
                  child: imagePath != null && imagePath!.isNotEmpty
                      ? Image.asset(imagePath ?? '')
                      : const Placeholder(),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    subtitle,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                price,
                style: Theme.of(context).textTheme.displayMedium?.apply(
                      color: Theme.of(context).primaryColor,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
